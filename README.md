YSF
===

YSF - kurta999's version

![alt tag](https://travis-ci.org/kurta999/YSF.svg?branch=YSF_)
![alt tag](https://ci.appveyor.com/api/projects/status/cbx4mw7ilgj5nrkr?svg=true)
![alt tag](https://img.shields.io/github/downloads/kurta999/YSF/total.svg?maxAge=86400)

YSF is a special addon, which purpose is to pull out maximum possibilities from the SA:MP Server, mainly with memory editing and hooking. 

**You can check my Lite version if you have problems with original YSF and you don't need deleted features.**


**Removed:**<br/>
native execute(const command[], saveoutput=0, index=0);<br/>
native ffind(const pattern[], filename[], len, &idx);<br/>
native frename(const oldname[], const newname[]);<br/>
native dfind(const pattern[], filename[], len, &idx);<br/>
native dcreate(const name[]);<br/>
native drename(const oldname[], const newname[]);<br/>
native SetModeRestartTime(Float:time);<br/>
native Float:GetModeRestartTime();<br/>
native SetMaxPlayers(maxplayers);<br/>
native SetMaxNPCs(maxnpcs);<br/>
native LoadFilterScript(const scriptname[]);<br/>
native UnLoadFilterScript(const scriptname[]);<br/>
native GetFilterScriptCount();<br/>
native GetFilterScriptName(id, name[], len = sizeof(name));<br/>
native AddServerRule(const name[], const value[], E_SERVER_RULE_FLAGS:flags = CON_VARFLAG_RULE);<br/>
native SetServerRule(const name[], const value[]);<br/>
native IsValidServerRule(const name[]);<br/>
native SetServerRuleFlags(const name[], E_SERVER_RULE_FLAGS:flags);<br/>
native E_SERVER_RULE_FLAGS:GetServerRuleFlags(const name[]);<br/>
native GetServerSettings(&showplayermarkes, &shownametags = 0, &stuntbonus = 0, &useplayerpedanims = 0, &bLimitchatradius = 0, &disableinteriorenterexits = 0, &nametaglos = 0, &manualvehicleengine = 0, &limitplayermarkers = 0, &vehiclefriendlyfire = 0, &defaultcameracollision = 0, &Float:fGlobalchatradius = 0.0, &Float:fNameTagDrawDistance = 0.0, &Float:fPlayermarkerslimit = 0.0);<br/>
native GetNPCCommandLine(npcid, npcscript[], length = sizeof(npcscript));<br/>
native GetSyncBounds(&Float:hmin, &Float:hmax = 0.0, &Float:vmin = 0.0, &Float:vmax = 0.0);<br/>
native SetSyncBounds(Float:hmin, Float:hmax, Float:vmin, Float:vmax);<br/>
native GetRCONCommandName(const cmdname[], changedname[], len = sizeof(changedname));<br/>
native ChangeRCONCommandName(const cmdname[], changedname[]);<br/>
native EnableConsoleMSGsForPlayer(playerid, color);<br/>
native DisableConsoleMSGsForPlayer(playerid);<br/>
native HasPlayerConsoleMessages(playerid, &color = 0);<br/>
native YSF_SetTickRate(ticks);<br/>
native YSF_GetTickRate();<br/>
native YSF_EnableNightVisionFix(enable);<br/>
native YSF_IsNightVisionFixEnabled();<br/>
native SetRecordingDirectory(const dir[]);<br/>
native GetRecordingDirectory(dir[], len = sizeof(dir));<br/>
native GetAvailableClasses();<br/>
native GetPlayerClass(classid, &teamid, &modelid = 0, &Float:spawn_x = 0.0, &Float:spawn_y = 0.0, &Float:spawn_z = 0.0, &Float:z_angle = 0.0, &weapon1 = 0, &weapon1_ammo = 0, &weapon2 = 0, &weapon2_ammo = 0, &weapon3 = 0, &weapon3_ammo = 0);<br/>
native EditPlayerClass(classid, teamid, modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, weapon1, weapon1_ammo, weapon2, weapon2_ammo, weapon3, weapon3_ammo);<br/>
native GetRunningTimers();<br/>
native SetPlayerGravity(playerid, Float:gravity);<br/>
native Float:GetPlayerGravity(playerid);<br/>
native SetPlayerAdmin(playerid, bool:admin);<br/>
native SetPlayerDisabledKeysSync(playerid, keys, updown = 0, leftright = 0);<br/>
native GetPlayerDisabledKeysSync(playerid, &keys, &updown = 0, &leftright = 0);<br/>
native GetPlayerBuildingsRemoved(playerid);<br/>
native GetActorSpawnInfo(actorid, &skinid, &Float:fX = 0.0, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fAngle = 0.0);<br/>
native GetActorSkin(actorid);<br/>
native GetActorAnimation(actorid, animlib[], animlibsize = sizeof(animlib), animname[], animnamesize = sizeof(animname), &Float:fDelta, &loop = 0, &lockx = 0, &locky = 0, &freeze = 0, &time = 0);<br/>
native TogglePlayerScoresPingsUpdate(playerid, bool:toggle);<br/>
native TogglePlayerFakePing(playerid, bool:toggle);<br/>
native SetPlayerFakePing(playerid, ping);<br/>
native SetPlayerNameInServerQuery(playerid, const name[]);<br/>
native GetPlayerNameInServerQuery(playerid, name[], len = sizeof(name));<br/>
native ResetPlayerNameInServerQuery(playerid);<br/>
native HideObjectForPlayer(playerid, objectid);<br/>
native ShowObjectForPlayer(playerid, objectid);<br/>
native HideNewObjectsForPlayer(playerid, bool:toggle);<br/>
native IsObjectHiddenForPlayer(playerid, objectid);<br/>
native NewObjectsHiddenForPlayer(playerid);<br/>
native Float:GetObjectDrawDistance(objectid);<br/>
native SetObjectMoveSpeed(objectid, Float:fSpeed);<br/>
native Float:GetObjectMoveSpeed(objectid);<br/>
native GetObjectTarget(objectid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native GetObjectAttachedData(objectid, &attached_vehicleid, &attached_objectid = 0, &attached_playerid = 0);<br/>
native GetObjectAttachedOffset(objectid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fRotX = 0.0, &Float:fRotY = 0.0, &Float:fRotZ = 0.0);<br/>
native IsObjectMaterialSlotUsed(objectid, materialindex);<br/> // Return values: 1 = material, 2 = material text
native GetObjectMaterial(objectid, materialindex, &modelid, txdname[], txdnamelen = sizeof(txdname), texturename[], texturenamelen = sizeof(texturename), &materialcolor = 0);<br/>
native GetObjectMaterialText(objectid, materialindex, text[], textlen = sizeof(text), &materialsize, fontface[], fontfacelen = sizeof(fontface), &fontsize = 0, &bold = 0, &fontcolor = 0, &backcolor = 0, &textalignment = 0);<br/>
native IsObjectNoCameraCol(objectid);<br/>
native Float:GetPlayerObjectDrawDistance(playerid, objectid);<br/>
native SetPlayerObjectMoveSpeed(playerid, objectid, Float:fSpeed);<br/>
native Float:GetPlayerObjectMoveSpeed(playerid, objectid);<br/>
native Float:GetPlayerObjectTarget(playerid, objectid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native GetPlayerObjectAttachedData(playerid, objectid, &attached_vehicleid, &attached_objectid = 0, &attached_playerid = 0);<br/>
native GetPlayerObjectAttachedOffset(playerid, objectid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fRotX = 0.0, &Float:fRotY = 0.0, &Float:fRotZ = 0.0);<br/>
native IsPlayerObjectMaterialSlotUsed(playerid, objectid, materialindex);<br/> // Return values: 1 = material, 2 = material text
native GetPlayerObjectMaterial(playerid, objectid, materialindex, &modelid, txdname[], txdnamelen = sizeof(txdname), texturename[], texturenamelen = sizeof(texturename), &materialcolor = 0);<br/>
native GetPlayerObjectMaterialText(playerid, objectid, materialindex, text[], textlen = sizeof(text), &materialsize, fontface[], fontfacelen = sizeof(fontface), &fontsize = 0, &bold = 0, &fontcolor = 0, &backcolor = 0, &textalignment = 0);<br/>
native IsPlayerObjectNoCameraCol(playerid, objectid);<br/>
native GetPlayerSurfingPlayerObjectID(playerid);<br/>
native GetPlayerCameraTargetPlayerObj(playerid);<br/>
native GetObjectType(playerid, objectid);<br/>
native GetPlayerAttachedObject(playerid, index, &modelid, &bone = 0, &Float:fX = 0.0, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fRotX = 0.0, &Float:fRotY = 0.0, &Float:fRotZ = 0.0, &Float:fSacleX = 0.0, &Float:fScaleY = 0.0, &Float:fScaleZ = 0.0, &materialcolor1 = 0, &materialcolor2 = 0);<br/>
native AttachPlayerObjectToObject(playerid, objectid, attachtoid, Float:OffsetX, Float:OffsetY, Float:OffsetZ, Float:RotX, Float:RotY, Float:RotZ, SyncRotation = 1);<br/>
native ClearBanList();<br/>
native IsBanned(const ipaddress[]);<br/>
native SetTimeoutTime(playerid, time_ms);<br/>
native GetLocalIP(index, localip[], len = sizeof(localip));<br/>
native IsValidGangZone(zoneid);<br/>
native IsPlayerInGangZone(playerid, zoneid);<br/>
native IsGangZoneVisibleForPlayer(playerid, zoneid);<br/>
native GangZoneGetColorForPlayer(playerid, zoneid);<br/>
native GangZoneGetFlashColorForPlayer(playerid, zoneid);<br/>
native IsGangZoneFlashingForPlayer(playerid, zoneid);<br/>
native GangZoneGetPos(zoneid, &Float:fMinX, &Float:fMinY = 0.0, &Float:fMaxX = 0.0, &Float:fMaxY = 0.0);<br/>
native CreatePlayerGangZone(playerid, Float:minx, Float:miny, Float:maxx, Float:maxy);<br/>
native PlayerGangZoneDestroy(playerid, zoneid);<br/>
native PlayerGangZoneShow(playerid, zoneid, color);<br/>
native PlayerGangZoneHide(playerid, zoneid);<br/>
native PlayerGangZoneFlash(playerid, zoneid, color);<br/>
native PlayerGangZoneStopFlash(playerid, zoneid);<br/>
native IsValidPlayerGangZone(playerid, zoneid);<br/>
native IsPlayerInPlayerGangZone(playerid, zoneid);<br/>
native IsPlayerGangZoneVisible(playerid, zoneid);<br/>
native PlayerGangZoneGetColor(playerid, zoneid);<br/>
native PlayerGangZoneGetFlashColor(playerid, zoneid);<br/>
native IsPlayerGangZoneFlashing(playerid, zoneid);<br/>
native PlayerGangZoneGetPos(playerid, zoneid, &Float:fMinX, &Float:fMinY = 0.0, &Float:fMaxX = 0.0, &Float:fMaxY = 0.0);<br/>
native IsValid3DTextLabel(Text3D:id);<br/>
native Is3DTextLabelStreamedIn(playerid, Text3D:id);<br/>
native Get3DTextLabelText(Text3D:id, text[], len = sizeof(text));<br/>
native Get3DTextLabelColor(Text3D:id);<br/>
native Get3DTextLabelPos(Text3D:id, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native Float:Get3DTextLabelDrawDistance(Text3D:id);<br/>
native Get3DTextLabelLOS(Text3D:id);<br/>
native Get3DTextLabelVirtualWorld(Text3D:id);<br/>
native Get3DTextLabelAttachedData(Text3D:id, &attached_playerid, &attached_vehicleid = 0);<br/>
native IsValidPlayer3DTextLabel(playerid, PlayerText3D:id);<br/>
native GetPlayer3DTextLabelText(playerid, PlayerText3D:id, text[], len = sizeof(text));<br/>
native GetPlayer3DTextLabelColor(playerid, PlayerText3D:id);<br/>
native GetPlayer3DTextLabelPos(playerid, PlayerText3D:id, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native Float:GetPlayer3DTextLabelDrawDist(playerid, PlayerText3D:id);<br/>
native GetPlayer3DTextLabelLOS(playerid, PlayerText3D:id);<br/>
native GetPlayer3DTextLabelVirtualW(playerid, PlayerText3D:id);<br/>
native GetPlayer3DTextLabelAttached(playerid, PlayerText3D:id, &attached_playerid, &attached_vehicleid = 0);<br/>
native IsMenuDisabled(Menu:menuid);<br/>
native IsMenuRowDisabled(Menu:menuid, row);<br/>
native GetMenuColumns(Menu:menuid);<br/>
native GetMenuItems(Menu:menuid, column);<br/>
native GetMenuPos(Menu:menuid, &Float:fX, &Float:fY = 0.0);<br/>
native GetMenuColumnWidth(Menu:menuid, &Float:fColumn1, &Float:fColumn2 = 0.0);<br/>
native GetMenuColumnHeader(Menu:menuid, column, header[], len = sizeof(header));<br/>
native GetMenuItem(Menu:menuid, column, itemid, item[], len = sizeof(item));<br/>
native IsValidPickup(pickupid);<br/>
native IsPickupStreamedIn(playerid, pickupid);<br/>
native GetPickupPos(pickupid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native GetPickupModel(pickupid);<br/>
native GetPickupType(pickupid);<br/>
native GetPickupVirtualWorld(pickupid);<br/>
native CreatePlayerPickup(playerid, model, type, Float:X, Float:Y, Float:Z, virtualworld = 0);<br/>
native DestroyPlayerPickup(playerid, pickupid);<br/>
native IsValidPlayerPickup(playerid, pickupid);<br/>
native IsPlayerPickupStreamedIn(playerid, pickupid);<br/>
native GetPlayerPickupPos(playerid, pickupid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);<br/>
native GetPlayerPickupModel(playerid, pickupid);<br/>
native GetPlayerPickupType(playerid, pickupid);<br/>
native GetPlayerPickupVirtualWorld(playerid, pickupid);<br/>
forward OnPlayerEnterGangZone(playerid, zoneid);<br/>
forward OnPlayerLeaveGangZone(playerid, zoneid);<br/>
forward OnPlayerEnterPlayerGangZone(playerid, zoneid);<br/>
forward OnPlayerLeavePlayerGangZone(playerid, zoneid);