/* YSF - kurta999's version */
/* Lite version made by critical99 (Using by Polski Serwer Imprezy) */

#if defined _YSF_included
	#endinput
#endif
#define _YSF_included

#define YSF_ERROR_ISNT_LOADED 					cellmin
#define YSF_ERROR_PARAMETER_COUNT_ISNT_EQUAL 	(cellmin + 1)
#define YSF_ERROR_PARAMETER_COUNT_ISNT_ENOUGH 	(cellmin + 2)

enum E_SCM_EVENT_TYPE
{
    SCM_EVENT_PAINTJOB = 1,
    SCM_EVENT_MOD = 2,
    SCM_EVENT_RESPRAY = 3,
    SCM_EVENT_MOD_SHOP = 4
}

// Per AMX function calling
native CallFunctionInScript(const scriptname[], const function[], const format[], {Float,_}:...);

// YSF settings
native YSF_ToggleOnServerMessage(toggle);
native YSF_IsOnServerMessageEnabled();
native YSF_SetExtendedNetStatsEnabled(enable);
native YSF_IsExtendedNetStatsEnabled();
native YSF_SetAFKAccuracy(time_ms);
native YSF_GetAFKAccuracy();

// Nickname
native IsValidNickName(const name[]);
native AllowNickNameCharacter(character, bool:allow);
native IsNickNameCharacterAllowed(character);

// Per player things
native SetPlayerTeamForPlayer(playerid, teamplayerid, teamid);
native GetPlayerTeamForPlayer(playerid, teamplayerid);
native SetPlayerSkinForPlayer(playerid, skinplayerid, skin);
native GetPlayerSkinForPlayer(playerid, skinplayerid);
native SetPlayerNameForPlayer(playerid, nameplayerid, const playername[]);
native GetPlayerNameForPlayer(playerid, nameplayerid, playername[], size = sizeof(playername));
native SetPlayerFightStyleForPlayer(playerid, styleplayerid, style);
native GetPlayerFightStyleForPlayer(playerid, skinplayerid);
native SetPlayerPosForPlayer(playerid, posplayerid, Float:fX, Float:fY, Float:fZ, bool:forcesync = true);
native SetPlayerRotationQuatForPlayer(playerid, quatplayerid, Float:w, Float:x, Float:y, Float:z, bool:forcesync = true);
native ApplyAnimationForPlayer(playerid, animplayerid, const animlib[], const animname[], Float:fDelta, loop, lockx, locky, freeze, time); // DOESN'T WORK
native GetPlayerWeather(playerid);
native GetSpawnInfo(playerid, &teamid, &modelid = 0, &Float:spawn_x = 0.0, &Float:spawn_y = 0.0, &Float:spawn_z = 0.0, &Float:z_angle = 0.0, &weapon1 = 0, &weapon1_ammo = 0, &weapon2 = 0, &weapon2_ammo = 0, &weapon3 = 0, &weapon3_ammo = 0);
native GetPlayerSkillLevel(playerid, skill);
native IsPlayerCheckpointActive(playerid);
native GetPlayerCheckpoint(playerid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fSize = 0.0);
native IsPlayerRaceCheckpointActive(playerid);
native GetPlayerRaceCheckpoint(playerid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fNextX = 0.0, &Float:fNextY = 0.0, &Float:fNextZ = 0.0, &Float:fSize = 0.0);
native GetPlayerWorldBounds(playerid, &Float:x_max, &Float:x_min = 0.0, &Float:y_max = 0.0, &Float:y_min = 0.0);
native IsPlayerInModShop(playerid);
native GetPlayerSirenState(playerid);
native GetPlayerLandingGearState(playerid);
native GetPlayerHydraReactorAngle(playerid);
native Float:GetPlayerTrainSpeed(playerid);
native Float:GetPlayerZAim(playerid);
native GetPlayerSurfingOffsets(playerid, &Float:fOffsetX, &Float:fOffsetY = 0.0, &Float:fOffsetZ = 0.0);
native GetPlayerRotationQuat(playerid, &Float:w, &Float:x = 0.0, &Float:y = 0.0, &Float:z = 0.0);
native GetPlayerDialogID(playerid);
native GetPlayerSpectateID(playerid);
native GetPlayerSpectateType(playerid);
native GetPlayerLastSyncedVehicleID(playerid);
native GetPlayerLastSyncedTrailerID(playerid);

native SendBulletData(senderid, forplayerid = -1, weaponid, hittype, hitid, Float:fHitOriginX, Float:fHitOriginY, Float:fHitOriginZ, Float:fHitTargetX, Float:fHitTargetY, Float:fHitTargetZ, Float:fCenterOfHitX, Float:fCenterOfHitY, Float:fCenterOfHitZ);
native ShowPlayerForPlayer(forplayerid, playerid, bool:setskin=false);
native HidePlayerForPlayer(forplayerid, playerid);
native AddPlayerForPlayer(forplayerid, playerid, isnpc = 0);
native RemovePlayerForPlayer(forplayerid, playerid);
native SetPlayerChatBubbleForPlayer(forplayerid, playerid, const text[], color, Float:drawdistance, expiretime);
native ResetPlayerMarkerForPlayer(playerid, resetplayerid);
native IsPlayerSpawned(playerid);
native IsPlayerControllable(playerid);
native SpawnForWorld(playerid);
native BroadcastDeath(playerid);
native IsPlayerCameraTargetEnabled(playerid);
native IsBuildingRemovedForPlayer(playerid, modelid, Float:fX, Float:fY, Float:fZ, Float:fRadius);

native TogglePlayerGhostMode(playerid, bool:toggle);
native GetPlayerGhostMode(playerid);
native SendPlayerDeath(playerid, forplayerid=-1);
native UpdatePlayerSyncData(playerid, type=-1);

// Pause functions
native IsPlayerPaused(playerid);
native GetPlayerPausedTime(playerid);

// Exclusive RPC broadcast
native SetExclusiveBroadcast(toggle);
native BroadcastToPlayer(playerid, toggle = 1);

// Vehicle functions
native GetVehicleSpawnInfo(vehicleid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0, &Float:fRot = 0.0, &color1 = 0, &color2 = 0);
native SetVehicleSpawnInfo(vehicleid, modelid, Float:fX, Float:fY, Float:fZ, Float:fAngle, color1, color2, respawntime = -2, interior = -2);
#if !defined GetVehicleColor
native GetVehicleColor(vehicleid, &color1, &color2 = 0);
#endif
#if !defined GetVehiclePaintjob
native GetVehiclePaintjob(vehicleid);
#endif
#if !defined GetVehicleInterior
native GetVehicleInterior(vehicleid);
#endif
native GetVehicleNumberPlate(vehicleid, plate[], len = sizeof(plate));
native SetVehicleRespawnDelay(vehicleid, delay);
native GetVehicleRespawnDelay(vehicleid);
native SetVehicleOccupiedTick(vehicleid, ticks);
native GetVehicleOccupiedTick(vehicleid); // GetTickCount() - GetVehicleOccupiedTick(vehicleid) = time passed since vehicle is occupied, in ms
native SetVehicleRespawnTick(vehicleid, ticks);
native GetVehicleRespawnTick(vehicleid); // GetTickCount() - GetVehicleRespawnTick(vehicleid) = time passed since vehicle spawned, in ms
native GetVehicleLastDriver(vehicleid);
native GetVehicleCab(vehicleid);
native HasVehicleBeenOccupied(vehicleid);
native SetVehicleBeenOccupied(vehicleid, occupied);
native IsVehicleOccupied(vehicleid);
native IsVehicleDead(vehicleid);
native SetVehicleDead(vehicleid, bool:dead);
native SetVehicleParamsSirenState(vehicleid, sirenState);
native ToggleVehicleSirenEnabled(vehicleid, enabled);
native IsVehicleSirenEnabled(vehicleid);
native GetVehicleMatrix(vehicleid, &Float:rightX, &Float:rightY = 0.0, &Float:rightZ = 0.0, &Float:upX = 0.0, &Float:upY = 0.0, &Float:upZ = 0.0, &Float:atX = 0.0, &Float:atY = 0.0, &Float:atZ = 0.0);
native GetVehicleModelCount(modelid);
native GetVehicleModelsUsed();

// Textdraw - Global
native IsValidTextDraw(Text:textdrawid);
native IsTextDrawVisibleForPlayer(playerid, Text:textdrawid);
native TextDrawGetString(Text:textdrawid, text[], len = sizeof(text));
native TextDrawSetPos(Text:textdrawid, Float:fX, Float:fY); // You can change textdraw pos with it, but you need to use TextDrawShowForPlayer() after that
native TextDrawGetLetterSize(Text:textdrawid, &Float:fX, &Float:fY = 0.0);
native TextDrawGetTextSize(Text:textdrawid, &Float:fX, &Float:fY = 0.0);
native TextDrawGetPos(Text:textdrawid, &Float:fX, &Float:fY = 0.0);
native TextDrawGetColor(Text:textdrawid);
native TextDrawGetBoxColor(Text:textdrawid);
native TextDrawGetBackgroundColor(Text:textdrawid);
native TextDrawGetShadow(Text:textdrawid);
native TextDrawGetOutline(Text:textdrawid);
native TextDrawGetFont(Text:textdrawid);
native TextDrawIsBox(Text:textdrawid);
native TextDrawIsProportional(Text:textdrawid);
native TextDrawIsSelectable(Text:textdrawid);
native TextDrawGetAlignment(Text:textdrawid);
native TextDrawGetPreviewModel(Text:textdrawid);
native TextDrawGetPreviewRot(Text:textdrawid, &Float:fRotX, &Float:fRotY = 0.0, &Float:fRotZ = 0.0, &Float:fZoom = 0.0);
native TextDrawGetPreviewVehCol(Text:textdrawid, &color1, &color2 = 0);
native TextDrawSetStringForPlayer(Text:textdrawid, playerid, const string[], {Float,_}:...);

// Textdraw - Player
native IsValidPlayerTextDraw(playerid, PlayerText:textdrawid);
native IsPlayerTextDrawVisible(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetString(playerid, PlayerText:textdrawid, text[], len = sizeof(text));
native PlayerTextDrawSetPos(playerid, PlayerText:textdrawid, Float:fX, Float:fY);
native PlayerTextDrawGetLetterSize(playerid, PlayerText:textdrawid, &Float:fX, &Float:fY = 0.0);
native PlayerTextDrawGetTextSize(playerid, PlayerText:textdrawid, &Float:fX, &Float:fY = 0.0);
native PlayerTextDrawGetPos(playerid, PlayerText:textdrawid, &Float:fX, &Float:fY = 0.0);
native PlayerTextDrawGetColor(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetBoxColor(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetBackgroundCol(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetShadow(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetOutline(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetFont(playerid, PlayerText:textdrawid);
native PlayerTextDrawIsBox(playerid, PlayerText:textdrawid);
native PlayerTextDrawIsProportional(playerid, PlayerText:textdrawid);
native PlayerTextDrawIsSelectable(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetAlignment(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetPreviewModel(playerid, PlayerText:textdrawid);
native PlayerTextDrawGetPreviewRot(playerid, PlayerText:textdrawid, &Float:fRotX, &Float:fRotY = 0.0, &Float:fRotZ = 0.0, &Float:fZoom = 0.0);
native PlayerTextDrawGetPreviewVehCol(playerid, PlayerText:textdrawid, &color1, &color2 = 0);

// Y_Less's model sizes inc
native GetColCount();
native Float:GetColSphereRadius(modelid);
native GetColSphereOffset(modelid, &Float:fX, &Float:fY = 0.0, &Float:fZ = 0.0);

// Formatting
native SendClientMessagef(playerid, color, const message[], {Float,_}:...);
native SendClientMessageToAllf(color, const message[], {Float,_}:...);
native GameTextForPlayerf(playerid, displaytime, style, const message[], {Float,_}:...);
native GameTextForAllf(displaytime, style, const message[], {Float,_}:...);
native SendPlayerMessageToPlayerf(playerid, senderid, const message[], {Float,_}:...);
native SendPlayerMessageToAllf(senderid, const message[], {Float,_}:...);
native SendRconCommandf(const command[], {Float,_}:...);

// Callbacks
forward OnPlayerPickUpPlayerPickup(playerid, pickupid);
forward OnPlayerPauseStateChange(playerid, pausestate);
forward OnPlayerStatsAndWeaponsUpdate(playerid);
forward OnRemoteRCONPacket(const ipaddr[], port, const password[], success, const command[]);
forward OnServerMessage(const msg[]);
forward OnPlayerClientGameInit(playerid, &usecjwalk, &limitglobalchat, &Float:globalchatradius, &Float:nametagdistance, &disableenterexits, &nametaglos, &manualvehengineandlights,
				&spawnsavailable, &shownametags, &showplayermarkers, &onfoot_rate, &incar_rate, &weapon_rate, &lacgompmode, &vehiclefriendlyfire);
forward OnOutcomeScmEvent(playerid, issuerid, E_SCM_EVENT_TYPE:eventid, vehicleid, arg1, arg2);
forward OnServerQueryInfo(const ipaddr[], hostname[51], gamemode[31], language[31]);
forward OnSystemCommandExecute(const line_output[], retval, index, success, line_current, line_total);

//////////////////////////////////////////////////////////////
// Fixes
//////////////////////////////////////////////////////////////

// No comment..
#if !defined IsValidVehicle
	native IsValidVehicle(vehicleid);
#endif

#if !defined GetWeather
	#define GetWeather() GetConsoleVarAsInt("weather")
#endif
native GetWeaponSlot(weaponid);

enum
{
	BS_BOOL,
	BS_CHAR,
	BS_UNSIGNEDCHAR,
	BS_SHORT,
	BS_UNSIGNEDSHORT,
	BS_INT,
	BS_UNSIGNEDINT,
	BS_FLOAT,
	BS_STRING
};

// Developer functions
native SendRPC(playerid, RPC, {Float,_}:...); // playerid == -1 -> broadcast
native SendData(playerid, {Float,_}:...); // playerid == -1 -> broadcast

/* EXAMPLE
CMD:pickup(playerid, params[])
{
	new
	    Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);

    SendRPC(playerid, 95, // rpcid
		BS_INT, strval(params), // int - pickupid
		BS_INT, 1222,    // int - modelid
		BS_INT, 19,     // int   - type
		BS_FLOAT, X + 2.0,   // float
		BS_FLOAT, Y,     // float
		BS_FLOAT, Z);    // float
	return 1;
}

CMD:destroypickup(playerid, params[])
{
	SendRPC(playerid, 63, // rpcid
		BS_INT, strval(params));    // int - pickupid
	return 1;
}
*/
