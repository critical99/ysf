/*
*  Version: MPL 1.1
*
*  The contents of this file are subject to the Mozilla Public License Version
*  1.1 (the "License"); you may not use this file except in compliance with
*  the License. You may obtain a copy of the License at
*  http://www.mozilla.org/MPL/
*
*  Software distributed under the License is distributed on an "AS IS" basis,
*  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
*  for the specific language governing rights and limitations under the
*  License.
*
*  The Original Code is the YSI 2.0 SA:MP plugin.
*
*  The Initial Developer of the Original Code is Alex "Y_Less" Cole.
*  Portions created by the Initial Developer are Copyright (C) 2008
*  the Initial Developer. All Rights Reserved. The development was abandobed
*  around 2010, afterwards kurta999 has continued it.
*
*  Contributor(s):
*
*	0x688, balika011, Gamer_Z, iFarbod, karimcambridge, Mellnik, P3ti, Riddick94
*	Slice, sprtik, uint32, Whitetigerswt, Y_Less, ziggi and complete SA-MP community
*
*  Special Thanks to:
*
*	SA:MP Team past, present and future
*	Incognito, maddinat0r, OrMisicL, Zeex
*
*/

#include <stdarg.h>
#include "includes/net.h"
#include "includes/platform.h"

#include "Hooks.h"
#include "CPlugin.h"
#include "CServer.h"
#include "CConfig.h"
#include "CFunctions.h"
#include "CCallbackManager.h"
#include "Memory.h"
#include "Natives.h"
#include "Globals.h"
#include "Utils.h"
#include "RPCs.h"
#include "subhook/subhook.h"

//----------------------------------------------------
subhook_t SetWeather_hook;
subhook_t Namecheck_hook;
subhook_t amx_Register_hook;
subhook_t logprintf_hook;
subhook_t query_hook;
subhook_t CVehicle__Respawn_hook;

// Callback hooks instead of using SAMP GDK
subhook_t CGameMode__OnPlayerConnect_hook;
subhook_t CGameMode__OnPlayerDisconnect_hook;
subhook_t CGameMode__OnPlayerSpawn_hook;
subhook_t CGameMode__OnPlayerStreamIn_hook;
subhook_t CGameMode__OnPlayerStreamOut_hook;
subhook_t CGameMode__OnDialogResponse_hook;

#ifdef SAMP_03DL
subhook_t ClientJoin_hook;
subhook_t AddSimpleModel_hook;
#endif

//----------------------------------------------------
char gRecordingDataPath[MAX_PATH];

///////////////////////////////////////////////////////////////
// Hooks //
///////////////////////////////////////////////////////////////

#ifdef _WIN32
#define HOOK_THISCALL(NAME, ARG1, ...) FASTCALL NAME(ARG1, void *_padding, __VA_ARGS__)
#else
#define HOOK_THISCALL(NAME, ARG1, ...) CDECL NAME(ARG1, ## __VA_ARGS__)
#endif

void HOOK_THISCALL(HOOK_CNetGame__SetWeather, void *thisptr, BYTE weatherid)
{
	subhook_remove(SetWeather_hook);

	auto &pool = CServer::Get()->PlayerPool;
	for (int i = 0; i != MAX_PLAYERS; ++i)
	{
		if (IsPlayerConnected(i))
			pool.Extra(i).byteWeather = weatherid;
	}

	CAddress::FUNC_CNetGame__SetWeather(thisptr, weatherid);
	subhook_install(SetWeather_hook);
}

//----------------------------------------------------

// Custom name check
bool HOOK_ContainsInvalidChars(char * szString)
{
	return !CPlugin::Get()->IsValidNick(szString);
}

//----------------------------------------------------

typedef BYTE (*FUNC_amx_Register)(AMX *amx, AMX_NATIVE_INFO *nativelist, int number);
int AMXAPI HOOK_amx_Register(AMX *amx, AMX_NATIVE_INFO *nativelist, int number)
{
	// amx_Register hook for redirect natives
	static bool bNativesHooked = false;

	if (!bNativesHooked && CPlugin::IsInitialized())
	{
		for(int i = 0; nativelist[i].name; i++)
		{
			if (ApplyHooks(nativelist[i])) bNativesHooked = true;

			if (i == number - 1) break;
		}
	}
	
	return ((FUNC_amx_Register)subhook_get_trampoline(amx_Register_hook))(amx, nativelist, number);
}

//----------------------------------------------------
FILE* g_fLog = NULL;

bool	bRconSocketReply = false;

SOCKET	cur_sock = INVALID_SOCKET;
char*	cur_data = NULL;
int		cur_datalen = 0;
sockaddr_in to;

//----------------------------------------------------

void RconSocketReply(char* szMessage);

//----------------------------------------------------

typedef void (*FUNC_logprintf)(const char *msg, ...);

void custom_logprintf(const char *msg, ...)
{
	char buffer[1024];
	va_list arguments;
	va_start(arguments, msg);
	vsnprintf(buffer, sizeof(buffer), msg, arguments);
	va_end(arguments);

	if(CCallbackManager::OnServerMessage(buffer))
	{
		reinterpret_cast<FUNC_logprintf>(subhook_get_trampoline(logprintf_hook))("%s", buffer);
	}
}

void *logprintf_trampoline()
{
	// If server messages aren't handled, the hook will jump to the trampoline
	if(CPlugin::Get()->IsOnServerMessageEnabled() && CPlugin::Get()->IsMainThread())
	{
		return reinterpret_cast<void*>(&custom_logprintf);
	}else{
		return subhook_get_trampoline(logprintf_hook);
	}
}

//----------------------------------------------------

void RconSocketReply(char* szMessage)
{
	// IMPORTANT!
	// Don't use logprintf from here... You'll cause an infinite loop.
	if (bRconSocketReply)
	{
		char* newdata = (char*)malloc(cur_datalen + strlen(szMessage) + sizeof(WORD));
		char* keep_ptr = newdata;
		memcpy(newdata, cur_data, cur_datalen);
		newdata += cur_datalen;
		*(WORD*)newdata = (WORD)strlen(szMessage);
		newdata += sizeof(WORD);
		memcpy(newdata, szMessage, strlen(szMessage));
		newdata += strlen(szMessage);
		sendto(cur_sock, keep_ptr, (int)(newdata - keep_ptr), 0, (sockaddr*)&to, sizeof(to));
		free(keep_ptr);
	}
}

//----------------------------------------------------
// bool CheckQueryFlood()
// returns 1 if this query could flood
// returns 0 otherwise
bool CheckQueryFlood(unsigned int binaryAddress)
{
	static DWORD dwLastQueryTick = 0;
	static unsigned int lastBinAddr = 0;

	if(!dwLastQueryTick)
	{
		dwLastQueryTick = static_cast<DWORD>(GetTickCount());
		lastBinAddr = binaryAddress;
		return 0;
	}
	if(lastBinAddr != binaryAddress)
	{
		if((static_cast<DWORD>(GetTickCount()) - dwLastQueryTick) < 25)
			return 1;

		dwLastQueryTick = static_cast<DWORD>(GetTickCount());
		lastBinAddr = binaryAddress;
	}
	return 0;
}

//----------------------------------------------------

void HOOK_THISCALL(HOOK_CVehicle__Respawn, CVehicle *thisptr)
{
	CSAMPFunctions::RespawnVehicle(thisptr);
}
//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnPlayerConnect, CGameMode *thisptr, cell playerid)
{
	subhook_remove(CGameMode__OnPlayerConnect_hook);

#ifndef NEW_PICKUP_SYSTEM
	CPlugin::Get()->AddPlayer(playerid);
#else
	// Initialize pickups
	if (CPlugin::Get()->AddPlayer(playerid))
		CPlugin::Get()->pPickupPool->InitializeForPlayer(playerid);
#endif

	int ret = CAddress::FUNC_CGameMode__OnPlayerConnect(thisptr, playerid);
	subhook_install(CGameMode__OnPlayerConnect_hook);
	return ret;
}

//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnPlayerDisconnect, CGameMode *thisptr, cell playerid, cell reason)
{
	subhook_remove(CGameMode__OnPlayerDisconnect_hook);

	CPlugin::Get()->RemovePlayer(playerid);
	
	int ret = CAddress::FUNC_CGameMode__OnPlayerDisconnect(thisptr, playerid, reason);
	subhook_install(CGameMode__OnPlayerDisconnect_hook);
	return ret;
}

//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnPlayerSpawn, CGameMode *thisptr, cell playerid)
{
	subhook_remove(CGameMode__OnPlayerSpawn_hook);

	if (IsPlayerConnected(playerid))
		CServer::Get()->PlayerPool.Extra(playerid).bControllable = true;

	int ret = CAddress::FUNC_CGameMode__OnPlayerSpawn(thisptr, playerid);
	subhook_install(CGameMode__OnPlayerSpawn_hook);
	return ret;
}

//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnPlayerStreamIn, CGameMode *thisptr, cell playerid, cell forplayerid)
{
	subhook_remove(CGameMode__OnPlayerStreamIn_hook);

	CPlugin::Get()->OnPlayerStreamIn(static_cast<WORD>(playerid), static_cast<WORD>(forplayerid));

	int ret = CAddress::FUNC_CGameMode__OnPlayerStreamIn(thisptr, playerid, forplayerid);
	subhook_install(CGameMode__OnPlayerStreamIn_hook);
	return ret;
}

//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnPlayerStreamOut, CGameMode *thisptr, cell playerid, cell forplayerid)
{
	subhook_remove(CGameMode__OnPlayerStreamOut_hook);

	CPlugin::Get()->OnPlayerStreamOut(static_cast<WORD>(playerid), static_cast<WORD>(forplayerid));
	
	int ret = CAddress::FUNC_CGameMode__OnPlayerStreamOut(thisptr, playerid, forplayerid);
	subhook_install(CGameMode__OnPlayerStreamOut_hook);
	return ret;
}

//----------------------------------------------------

int HOOK_THISCALL(HOOK_CGameMode__OnDialogResponse, CGameMode *thisptr, cell playerid, cell dialogid, cell response, cell listitem, char *szInputtext)
{
	subhook_remove(CGameMode__OnDialogResponse_hook);

	int ret = -1;
	if (IsPlayerConnected(playerid))
	{
		auto &data = CServer::Get()->PlayerPool.Extra(playerid);
		if (CConfig::Get()->m_bDialogProtection && data.wDialogID != dialogid)
		{
			logprintf("YSF: Might dialog hack has been detected for player %s(%d) - which should be: %d, dialogid: %d", GetPlayerName(playerid), playerid, data.wDialogID, dialogid);
			ret = 1;
		}
		data.wDialogID = -1;
	}

	if(ret == -1)
		ret = CAddress::FUNC_CGameMode__OnDialogResponse(thisptr, playerid, dialogid, response, listitem, szInputtext);
	
	subhook_install(CGameMode__OnDialogResponse_hook);
	return ret;
}

//----------------------------------------------------

#ifdef SAMP_03DL
void CDECL HOOK_ClientJoin(RPCParameters *rpcParams)
{
	//4057 - 0.3.7-R2, 4062 - 0.3.DL-R1
	subhook_remove(ClientJoin_hook);

	/*int *ver = (int*)rpcParams->input;
	logprintf("Client joined with version %d.", *ver);

	unsigned char namelen = rpcParams->input[5];

	unsigned int *resp = (unsigned int*)rpcParams->input+6+namelen;
	logprintf("Resp %d.", *resp);
	*resp = *resp ^ *ver ^ 4062;
	*ver = 4062;*/

	CAddress::FUNC_ClientJoin(rpcParams);
	
	subhook_install(ClientJoin_hook);
}

int HOOK_THISCALL(HOOK_AddSimpleModel, CArtInfo *pArtInfo, MODEL_TYPE type, int virtualworld, int baseid, int newid, char *dffname, char *txdname, char timeon, char timeoff)
{
	CPlugin &plugin = *CPlugin::Get();

	CModelInfo *model = plugin.FindCachedModelInfo(dffname, txdname);

	int result;
	if (model == nullptr)
	{
		subhook_remove(AddSimpleModel_hook);
		result = CAddress::FUNC_AddSimpleModel(pArtInfo, type, virtualworld, baseid, newid, dffname, txdname, timeon, timeoff);
		subhook_install(AddSimpleModel_hook);

		if (result > 0 || (result == 0 && pArtInfo->artList.dwCapacity > 0 && pArtInfo->artList.pModelList[0]->dwNewId == newid))
		{
			model = pArtInfo->artList.pModelList[result];
			plugin.CacheModelInfo(model);
		}
	} else {
		// server will never delete this
		CModelInfo *newModel = new CModelInfo(*model);
		newModel->bType = type;
		newModel->dwVirtualWorld = virtualworld;
		newModel->dwBaseId = baseid;
		newModel->dwNewId = newid;
		newModel->bTimeOn = timeon;
		newModel->bTimeOff = timeoff;
		result = CAddress::FUNC_DynamicListInsert(&pArtInfo->artList, newModel);
	}

	return result;
}
#endif

//----------------------------------------------------

template <class TFunc, class THook>
subhook_t Hook(ADDR<TFunc> &func, THook hook)
{
	//logprintf("Hooking %x", (DWORD)func);
	subhook_t var = subhook_new(reinterpret_cast<void*>(*func), reinterpret_cast<void*>(hook), static_cast<subhook_options_t>(0));
	subhook_install(var);
	return var;
}

// Things that needs to be hooked before netgame initialied
void InstallPreHooks()
{
	SetWeather_hook = Hook(CAddress::FUNC_CNetGame__SetWeather, HOOK_CNetGame__SetWeather);
	Namecheck_hook = Hook(CAddress::FUNC_ContainsInvalidChars, HOOK_ContainsInvalidChars);
	amx_Register_hook = subhook_new(reinterpret_cast<void*>(((FUNC_amx_Register*)pAMXFunctions)[PLUGIN_AMX_EXPORT_Register]), reinterpret_cast<void*>(HOOK_amx_Register), static_cast<subhook_options_t>(NULL));
	subhook_install(amx_Register_hook);

	CVehicle__Respawn_hook = Hook(CAddress::FUNC_CVehicle__Respawn, HOOK_CVehicle__Respawn);
	
#ifdef SAMP_03DL
	//ClientJoin_hook = Hook(CAddress::FUNC_ClientJoin, HOOK_ClientJoin);
	//AddSimpleModel_hook = Hook(CAddress::FUNC_AddSimpleModel, HOOK_AddSimpleModel);
#endif
	
	// Callback hooks
	CGameMode__OnPlayerConnect_hook = Hook(CAddress::FUNC_CGameMode__OnPlayerConnect, HOOK_CGameMode__OnPlayerConnect);
	CGameMode__OnPlayerDisconnect_hook = Hook(CAddress::FUNC_CGameMode__OnPlayerDisconnect, HOOK_CGameMode__OnPlayerDisconnect);
	CGameMode__OnPlayerSpawn_hook = Hook(CAddress::FUNC_CGameMode__OnPlayerSpawn, HOOK_CGameMode__OnPlayerSpawn);
	CGameMode__OnPlayerStreamIn_hook = Hook(CAddress::FUNC_CGameMode__OnPlayerStreamIn, HOOK_CGameMode__OnPlayerStreamIn);
	CGameMode__OnPlayerStreamOut_hook = Hook(CAddress::FUNC_CGameMode__OnPlayerStreamOut, HOOK_CGameMode__OnPlayerStreamOut);
	CGameMode__OnDialogResponse_hook = Hook(CAddress::FUNC_CGameMode__OnDialogResponse, HOOK_CGameMode__OnDialogResponse);
	
	if(CAddress::ADDR_RecordingDirectory)
	{
		strcpy(gRecordingDataPath, "scriptfiles/%s.rec");
		CAddress::ADDR_RecordingDirectory.unlock();
		*CAddress::ADDR_RecordingDirectory = gRecordingDataPath;
	}
}

extern "C" void *subhook_unprotect(void *address, size_t size);

// Things that needs to be hooked after netgame initialied
void InstallPostHooks()
{
	CSAMPFunctions::PostInitialize();

	if (CConfig::Get()->m_bIncreaseRakNetInternalPlayers)
		CSAMPFunctions::Start(MAX_PLAYERS, 0, CConfig::Get()->m_iRakNetInternalSleepTime, static_cast<unsigned short>(CSAMPFunctions::GetIntVariable("port")), CSAMPFunctions::GetStringVariable("bind"));

#ifdef NEW_PICKUP_SYSTEM
	// Recreate Pickup pool
	CPlugin::Get()->pPickupPool = new CYSFPickupPool();
#endif
	// Re-init a few RPCs
	InitRPCs();

	static unsigned char HOOK_logprintf[7] = {0xE8, 0xCC, 0xCC, 0xCC, 0xCC, 0xFF, 0xE0}; //call rel, jmp eax
	*reinterpret_cast<uintptr_t*>(HOOK_logprintf + 1) = reinterpret_cast<uintptr_t>(&logprintf_trampoline) - reinterpret_cast<uintptr_t>(HOOK_logprintf + 5);
	subhook_unprotect(HOOK_logprintf, sizeof(HOOK_logprintf));

	logprintf_hook = subhook_new(reinterpret_cast<void*>(ppPluginData[PLUGIN_DATA_LOGPRINTF]), HOOK_logprintf, static_cast<subhook_options_t>(NULL));
	subhook_install(logprintf_hook);

	// logprintf("YSF - pNetGame: 0x%X, pConsole: 0x%X, pRakServer: 0x%X", pNetGame, pConsole, pRakServer);
}

void UninstallHooks()
{
	SUBHOOK_REMOVE(SetWeather_hook);
	SUBHOOK_REMOVE(Namecheck_hook);
	SUBHOOK_REMOVE(amx_Register_hook);
	SUBHOOK_REMOVE(query_hook);
	SUBHOOK_REMOVE(logprintf_hook);
	SUBHOOK_REMOVE(CVehicle__Respawn_hook);
	SUBHOOK_REMOVE(CGameMode__OnPlayerConnect_hook);
	SUBHOOK_REMOVE(CGameMode__OnPlayerDisconnect_hook);
	SUBHOOK_REMOVE(CGameMode__OnPlayerSpawn_hook);
	SUBHOOK_REMOVE(CGameMode__OnPlayerStreamIn_hook);
	SUBHOOK_REMOVE(CGameMode__OnPlayerStreamOut_hook);
	SUBHOOK_REMOVE(CGameMode__OnDialogResponse_hook);

	// Close log file
	if (g_fLog)
	{
		fclose(g_fLog);
		g_fLog = NULL;
	}
}
