#include "../Natives.h"
#include "../CPlugin.h"
#include "../CScriptParams.h"
#include "../Utils.h"

#include "raknet/NetworkTypes.h"

namespace Natives
{
	// native SendRPC(playerid, RPC, {Float,_}:...)
	AMX_DECLARE_NATIVE(SendRPC)
	{
		if (!CPlugin::Get()->IsInitialized()) return std::numeric_limits<int>::lowest(); // If unknown server version

		const bool bBroadcast = static_cast<int>(params[1]) == -1;
		BYTE rpcid = static_cast<BYTE>(params[2]);

		PlayerID playerId = bBroadcast ? UNASSIGNED_PLAYER_ID : CSAMPFunctions::GetPlayerIDFromIndex(static_cast<int>(params[1]));

		if (playerId.binaryAddress == UNASSIGNED_PLAYER_ID.binaryAddress && !bBroadcast)
			return 0;

		RakNet::BitStream bs;
		cell *type = (cell*)0;
		cell *data = (cell*)0;

		for (int i = 0; i < (int)((params[0] / sizeof(cell)) - 2); i += 2)
		{
			amx_GetAddr(amx, params[i + 3], &type);
			amx_GetAddr(amx, params[i + 4], &data);

			if (type && data)
			{
				switch (*type)
				{
				case BS_BOOL:
					bs.Write((bool)(*data != 0));
					break;
				case BS_CHAR:
					bs.Write(*(char*)data);
					break;
				case BS_UNSIGNEDCHAR:
					bs.Write(*(unsigned char*)data);
					break;
				case BS_SHORT:
					bs.Write(*(short*)data);
					break;
				case BS_UNSIGNEDSHORT:
					bs.Write(*(unsigned short*)data);
					break;
				case BS_INT:
					bs.Write(*(int*)data);
					break;
				case BS_UNSIGNEDINT:
					bs.Write(*(unsigned int*)data);
					break;
				case BS_FLOAT:
					bs.Write(*(float*)data);
					break;
				case BS_STRING:
				{
					int len;
					amx_StrLen(data, &len);
					len++;
					char* str = new char[len];
					amx_GetString(str, data, 0, len);
					bs.Write(str, len - 1);
					//logprintf("str: %s", str);
					delete[] str;
				}
				break;
				}
			}
		}

		if (bBroadcast)
		{
			CSAMPFunctions::RPC(&rpcid, &bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_PLAYER_ID, true, 0);
		}
		else
		{
			CSAMPFunctions::RPC(&rpcid, &bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, playerId, 0, 0);
		}
		return 1;
	}

	// native SendData(playerid, {Float,_}:...)
	AMX_DECLARE_NATIVE(SendData)
	{
		if (!CPlugin::Get()->IsInitialized()) return std::numeric_limits<int>::lowest(); // If unknown server version

		const bool bBroadcast = static_cast<int>(params[1]) == -1;
		const PlayerID playerId = bBroadcast ? UNASSIGNED_PLAYER_ID : CSAMPFunctions::GetPlayerIDFromIndex(static_cast<int>(params[1]));

		if (playerId.binaryAddress == UNASSIGNED_PLAYER_ID.binaryAddress && !bBroadcast)
			return 0;

		RakNet::BitStream bs;
		cell *type = (cell*)0;
		cell *data = (cell*)0;

		for (int i = 0; i < (int)((params[0] / sizeof(cell)) - 2); i += 2)
		{
			amx_GetAddr(amx, params[i + 2], &type);
			amx_GetAddr(amx, params[i + 3], &data);

			if (type && data)
			{
				switch (*type)
				{
				case BS_BOOL:
					bs.Write((bool)(*data != 0));
					break;
				case BS_CHAR:
					bs.Write(*(char*)data);
					break;
				case BS_UNSIGNEDCHAR:
					bs.Write(*(unsigned char*)data);
					break;
				case BS_SHORT:
					bs.Write(*(short*)data);
					break;
				case BS_UNSIGNEDSHORT:
					bs.Write(*(unsigned short*)data);
					break;
				case BS_INT:
					bs.Write(*(int*)data);
					break;
				case BS_UNSIGNEDINT:
					bs.Write(*(unsigned int*)data);
					break;
				case BS_FLOAT:
					bs.Write(*(float*)data);
					break;
				case BS_STRING:
				{
					int len;
					amx_StrLen(data, &len);
					len++;
					char* str = new char[len];
					amx_GetString(str, data, 0, len);
					bs.Write(str, len - 1);
					//logprintf("str: %s", str);
					delete[] str;
				}
				break;
				}
			}
		}

		if (bBroadcast)
		{
			CSAMPFunctions::Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_PLAYER_ID, true);
		}
		else
		{
			CSAMPFunctions::Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, playerId, 0);
		}
		return 1;
	}
}

static AMX_NATIVE_INFO native_list[] =
{
	// RakServer functions
	AMX_DEFINE_NATIVE(SendRPC)
	AMX_DEFINE_NATIVE(SendData)
};

void RakNetLoadNatives()
{
	RegisterNatives(native_list);
}
